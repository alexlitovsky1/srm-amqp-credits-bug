package com.alexlitovsky.bugs.amqp;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.scheduler.Scheduled;

@ApplicationScoped
public class MessageSender {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageSender.class);
	
	@Inject
    @Channel("test1")
    Emitter<String> emitter1;
	
	@Inject
    @Channel("test2")
    Emitter<String> emitter2;
	
	@Inject
    @Channel("test3")
    Emitter<String> emitter3;

	@Scheduled(every = "{sender.period}", delayed = "5s")
	public void sendMessage() {
		sendMessage(emitter1, "test1");
		sendMessage(emitter2, "test2");
		sendMessage(emitter3, "test3");
	}
	
	private static void sendMessage(Emitter<String> emitter, String channelName) {
		try {
			LOGGER.info("sending message to " + channelName + "...");
			emitter.send("Hello world").toCompletableFuture().get(1, TimeUnit.SECONDS);
			LOGGER.info("sent message to " + channelName);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			throw new RuntimeException("Timeout sending to " + channelName, e);
		}
	}
}
